# OpenWRT Cheat Sheet

## diffconfigs
all diffconfigs are under `diffconfigs/`

* `tl-wr841-v11`: minimal TL-WR841 v11 with wireguard and prometheus node exporters
* `rpi_model_b`: minimal raspberry pi model b with wireguard and ddns-scripts (w. https)

## build from source

information from
[Build system usage](https://openwrt.org/docs/guide-developer/build-system/use-buildsystem) and
[Quick Image Building Guide](https://openwrt.org/docs/guide-developer/quickstart-build-images)

### 1. get the right revision

```sh
$ git fetch origin
$ git [verify-tag|verify-commit] <revison>
$ git checkout <revision>
```

### 2. clean build environment

```sh
$ make clean
$ make dirclean		# if switching architectures or something
$ make distclean	# to nuke everything
```

### 3. update feeds

```sh
$ ./scripts/feeds update -a
$ ./scripts/feeds install -a
```

### 4. configure target

```sh
$ make menuconfig					# to configure via ncurses

$ cp ../diffconfig-something .config			# to use diffconfig saved somewhere
$ make defconfig					# to use diffconfig saved somewhere

$ ./scripts/diffconfig > ../diffconfig-something 	# to save diffconfig
```

### 5. build

```sh
$ make download # pre download all sources
$ make -j3	# build with 3 cores or threads (ideal: cpu count + 1)
```

### 6. enjoy

enjoy your build in `bin/targets/.../`
